<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trail;
use App\Models\User;
use App\Notifications\SolverNotification;
use Illuminate\Support\Facades\Validator;
use Exception;

class SolverController extends Controller
{
    public $validator;

    public function findValue (Request $request)
    {
        $this->validator = Validator::make($request->all(), [
            'valA'=>'nullable|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'valB' => 'nullable|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'valC' => 'nullable|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
            'valTheta' => 'nullable|max:22|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/'
        ]);
        if ($this->validator->fails()) {
            throw new Exception($this->validator->errors());
        }

        $a = (float) $request->input('valA');
        $b = (float) $request->input('valB');
        $hyp = (float) $request->input('valC');
        $theta = (float) $request->input('valTheta');

        $resultHtml = array();

        if ($hyp && $b) {
            $a = $this->pythagoreanCalc(null, $b, $hyp);
            $resultHtml[] = $this->makeResult('A', number_format($a, 2) );

            $theta = $this->convertToDegrees(acos($b / $hyp));
            $resultHtml[] = $this->makeResult('Theta', number_format($theta, 2) );

        } else if ($hyp && $a) {
            $b = $this->pythagoreanCalc($a, null, $hyp);
            $resultHtml[] = $this->makeResult('B', number_format($b, 2) );
            
            $theta = $this->convertToDegrees(asin($a / $hyp));
            $resultHtml[] = $this->makeResult('Theta', number_format($theta, 2) );
        } else if ($a && $b) {
            $hyp = $this->pythagoreanCalc($a, $b, null);
            $resultHtml[] = $this->makeResult('C', number_format($hyp, 2) );
           
            $theta = $this->convertToDegrees(atan($a / $b));
            $resultHtml[] = $this->makeResult('Theta', number_format($theta, 2) );

        } else {
            // Assuming Theta is given
            if ($a) {
                $hyp = $a / sin($this->convertToRadian($theta));
                $resultHtml[] = $this->makeResult('C', number_format($hyp, 2) );

                $b = $this->pythagoreanCalc($a, null, $hyp);
                $resultHtml[] = $this->makeResult('B', number_format($b, 2) );
            } else if ($b) {
                $hyp = $b / cos($this->convertToRadian($theta));
                $resultHtml[] = $this->makeResult('C', number_format($hyp, 2) );

                $a = $this->pythagoreanCalc(null, $b, $hyp);
                $resultHtml[] = $this->makeResult('A', number_format($a, 2) );
            } else {
                $a = sin($this->convertToRadian($theta) ) * $hyp;
                $resultHtml[] = $this->makeResult('A', number_format($a, 2) );
                
                $b = $this->pythagoreanCalc($a, null, $hyp);
                $resultHtml[] = $this->makeResult('B', number_format($b, 2) );
            }
        }

        /**
         * Save to trail table with auto increment ID
         */
        $trail = new Trail();
        $trail->a = $a;
        $trail->b = $b;
        $trail->c = $hyp;
        $trail->theta = $theta;
        $trail->save();

        $user = User::first();
        $user->notify(new SolverNotification(Trail::get()));
 
        return $resultHtml;
    }
    
    public function convertToDegrees($radians) 
    {
        return ($radians * (180 / pi() ));
    }

    public function convertToRadian($degrees) 
    {
        return $degrees * (pi() / 180);
    }

    public function pythagoreanCalc($a, $b, $hyp) 
    {
        // Using Pythagorean Theorem
        if (!$a) {
            return sqrt(pow($hyp,2) - pow($b,2));
        } else if (!$b) {
            return sqrt(pow($hyp,2) - pow($a,2));
        } else {
            // Assume looking for hypotenuse
            return sqrt(pow($a,2) + pow($b,2));
        }
    }

    public function makeResult($type, $value)
    {
        switch($type)
        {
            case 'A': 
                return 'Side A = '. $value;
                break;
            case 'B': 
                return 'Side B = '. $value;
                break;
            case 'C': 
                return 'Side C (Hypotenuse) = '. $value;
                break;
            case 'Theta': 
                return 'Theta (degrees) = '. $value;
                break;
        }
    }
}
