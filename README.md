<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## Necessaries steps for getting started into the system

It is necessary to perform a few things in order to the system work properly 

- Install Latest PHP version (It is necessary so that you can use PHP Server)
- Install Composer
- Install Node
- Install MySQL

## Some extra steps

#### Update these settings in the .env file:

-   DB_DATABASE (your local database, i.e. "todo")
-   DB_USERNAME (your local db username, i.e. "root")
-   DB_PASSWORD (your local db password, i.e. "")
-   MAIL_MAILER (your mail credentials, i.e. "smtp.mailtrap.io")
-   MAIL_USERNAME (your mail credentials, i.e. "username")
-   MAIL_PASSWORD (your mail credentials, i.e. "password")
-   MAIL_FROM_ADDRESS (your mail credentials, i.e. "mchuatak@yahoo.com")

#### Install PHP dependencies:

```bash
composer install
```

#### Install Javascript dependencies:

```bash
npm install
```

_If you don't have Node and NPM installed, [instructions here](https://www.npmjs.com/get-npm)._

#### Run an initial build:

```bash
npm run dev
```

#### Run the database migrations:

```bash
php artisan migrate
```

#### Database Seeding

If you need sample data to work with, you can seed the database:

```
php artisan migrate:refresh --seed --force
```

#### Run the application:

```bash
php artisan serve
```

## Right Triangle Solver 
- The application will only allow the user to enter two numberic values for a, b, c or theta affecting values (a, b, c or theta) with up to 2 decimal places. 
- The application will record the inputs/result into the table (Trail), after clicking the "Calculate" button. 
- It also support to send a notification message (email)

## Online Calculator

This is calculator I am using to verify the result if it's correct. 
[Click here](http://www.cleavebooks.co.uk/scol/calrtri.htm?fbclid=IwAR3yB9YuG2vCiuo1Bb1TvYqXruS3xRaa88Ll8DZGyzhV4hO2TT3v73Zz0ng)