@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Calculated Results
            <style type="text/css">
            .content table {
                margin: 30px 0 50px 7.5%;
                width: 85%!important;
                border: 1px solid #ececed;
                border-collapse: collapse;
                border-bottom: 0;
            }
            table {
                border-collapse: collapse;
                border-spacing: 0;
                empty-cells: show;
            }
            .content table thead tr, .content table tr.header {
                background: #ececed;
            }
            .content table tr.header {
                background: #ececed;
            }            
            h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, cite, code, del, dfn, em, img, q, s, samp, small, strike, strong, sub, sup, tt, var, dd, dl, dt, li, ol, ul, fieldset, form, label, legend, button, table, caption, tbody, tfoot, thead, tr, th, td {
                margin: 0;
                padding: 0;
                border: 0;
                font-weight: normal;
                font-style: normal;
                font-size: 100%;
                line-height: 1;
                font-family: inherit;
            }        
            thead {
                display: table-header-group;
                vertical-align: middle;
                border-color: inherit;
            }   
            .content table thead tr td, .content table tr.header td {
                padding: 10px 0 10px 6px;
                font-size: 1.0em;
                font-weight: bold;
                color: #111324;
            }
            .content table tr.header td {
                padding: 10px 0 10px 6px;
                font-size: 1.0em;
                font-weight: bold;
                color: #111324;
            }
            .content table td {
                border-bottom: 1px solid #ececed;
                text-align: left;
                padding: 10px 6px;
                font-size: 1.0em;
            }             
            </style>
        @endcomponent
    @endslot

{{-- Body --}}
<table class="forms" style="width: 100%;" width="100%" cellspacing="0" cellpadding="0">
<thead>
<tr class="header">
<td class="element-header" width="10%">ID</td>
<td class="client" width="15%">Theta</td>
<td class="client" width="15%">A</td>
<td class="client" width="15%">B</td>
<td class="client" width="15%">C</td>
<td class="client" width="15%">Created Date</td>
<td class="client" width="15%">Date Modified</td>
</tr>
</thead>
<tbody>
@foreach ($user as $trail)
<tr>
    <td class="element-style">{{ $trail->id }}</td>
    <td class="element-style">{{ $trail->theta }}</td>
    <td class="element-style">{{ $trail->a }}</td>
    <td class="element-style">{{ $trail->b }}</td>
    <td class="element-style">{{ $trail->c }}</td>
    <td class="element-style">{{ $trail->created_at->format('d/m/Y') }}</td>
    <td class="element-style">{{ $trail->updated_at->format('d/m/Y') }}</td>
</tr>
@endforeach
</tbody>
</table>

{{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}. Super FOOTER!
        @endcomponent
    @endslot
@endcomponent